package com.luisbalam.mispeliculas;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import android.support.v4.content.LocalBroadcastManager;

public class BaseDatosMisPeliculas extends SQLiteOpenHelper {

    private static final int VERSION_BASEDATOS = 1;
    private static final String NOMBRE_BASEDATOS = "peliculas.db";
    private static final String NOMBRE_TABLA = "peliculas";
    public static final String COLUMNA_NOMBRE = "nombrePelicula";
    public static final String COLUMNA_DESCRIPCION = "descripcionPelicula";
    public static final String COLUMNA_FORMATO = "formatoPelicula";
    public static final String COLUMNA_PRECIO = "precioPelicula";
    public static final String COLUMNA_EXISTENCIA = "existenciaPelicula";

    public BaseDatosMisPeliculas(Context context) {

        super(context, NOMBRE_BASEDATOS, null, VERSION_BASEDATOS);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String createQuery =
                "CREATE TABLE " + NOMBRE_TABLA +
                        " (_id INTEGER PRIMARY KEY, "
                        + COLUMNA_NOMBRE + " TEXT NOT NULL COLLATE UNICODE, "
                        + COLUMNA_DESCRIPCION + " TEXT NOT NULL, "
                        + COLUMNA_FORMATO + " TEXT, "
                        + COLUMNA_PRECIO + " TEXT, "
                        + COLUMNA_EXISTENCIA + " TEXT)";

        db.execSQL(createQuery);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        String upgradeQuery = "DROP TABLE IF EXIST "+ NOMBRE_TABLA;
        db.execSQL(upgradeQuery);
    }

    public static long insertaAnimal (Context context, String nombre, String descripcion, String formato, String precio, String existencia){

        SQLiteOpenHelper dbOpenHelper = new BaseDatosMisPeliculas(context);
        SQLiteDatabase database = dbOpenHelper.getWritableDatabase();

        ContentValues valorPelicula = new ContentValues();
        valorPelicula.put(COLUMNA_NOMBRE, nombre);
        valorPelicula.put(COLUMNA_DESCRIPCION, descripcion);
        valorPelicula.put(COLUMNA_FORMATO, formato);
        valorPelicula.put(COLUMNA_PRECIO, precio);
        valorPelicula.put(COLUMNA_EXISTENCIA, existencia);

        long result = -1L;
        try {
            result = database.insert(NOMBRE_TABLA, null, valorPelicula);
            if (result != -1L){

                LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(context);
                Intent intentFilter = new Intent(CargarPeliculas.ACTION_RELOAD_TABLE);
                broadcastManager.sendBroadcast(intentFilter);
            }
        } finally {
            dbOpenHelper.close();
        }
        return result;
    }

    public static Cursor devuelveTodos (Context context){
        SQLiteOpenHelper dbOpenHelper = new BaseDatosMisPeliculas(context);
        SQLiteDatabase database = dbOpenHelper.getReadableDatabase();

        return  database.query(
                NOMBRE_TABLA,
                new String[]{COLUMNA_NOMBRE, COLUMNA_DESCRIPCION, COLUMNA_FORMATO, COLUMNA_PRECIO, COLUMNA_EXISTENCIA, BaseColumns._ID},
                null, null, null, null,
                COLUMNA_NOMBRE+" ASC");
    }

    public static Cursor devuelveConId(Context context, long identificador){
        SQLiteOpenHelper dbOpenHelper = new BaseDatosMisPeliculas(context);
        SQLiteDatabase database = dbOpenHelper.getReadableDatabase();

        return database.query(
                NOMBRE_TABLA,
                new String[]{COLUMNA_NOMBRE, COLUMNA_DESCRIPCION, COLUMNA_FORMATO, COLUMNA_PRECIO, COLUMNA_EXISTENCIA, BaseColumns._ID},
                BaseColumns._ID + " = ?",
                new String[]{String.valueOf(identificador)},
                null,
                null,
                COLUMNA_NOMBRE+" ASC");
    }

    public static int eliminaConId(Context context, long animalId){

        SQLiteOpenHelper dbOpenHelper = new BaseDatosMisPeliculas(context);
        SQLiteDatabase database = dbOpenHelper.getWritableDatabase();

        int resultado = database.delete(
                NOMBRE_TABLA,
                BaseColumns._ID + " =?",
                new String[]{String.valueOf(animalId)});

        if (resultado != 0){

            LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(context);
            Intent intentFilter = new Intent(CargarPeliculas.ACTION_RELOAD_TABLE);
            broadcastManager.sendBroadcast(intentFilter);
        }

        dbOpenHelper.close();
        return resultado;

    }

    public static int actualizaAnimal (Context context, String nombre, String descripcion, String formato, String precio, String existencia, long animalId){

        SQLiteOpenHelper dbOpenHelper = new BaseDatosMisPeliculas(context);
        SQLiteDatabase database = dbOpenHelper.getWritableDatabase();

        ContentValues valorAnimal = new ContentValues();
        valorAnimal.put(COLUMNA_NOMBRE, nombre);
        valorAnimal.put(COLUMNA_DESCRIPCION, descripcion);
        valorAnimal.put(COLUMNA_FORMATO, formato);
        valorAnimal.put(COLUMNA_PRECIO, precio);
        valorAnimal.put(COLUMNA_EXISTENCIA, existencia);

        int result = database.update(
                NOMBRE_TABLA,
                valorAnimal,
                BaseColumns._ID + " =?",
                new String[]{String.valueOf(animalId)});

        if (result != 0){

            LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(context);
            Intent intentFilter = new Intent(CargarPeliculas.ACTION_RELOAD_TABLE);
            broadcastManager.sendBroadcast(intentFilter);
        }

        dbOpenHelper.close();

        return result;
    }
}

