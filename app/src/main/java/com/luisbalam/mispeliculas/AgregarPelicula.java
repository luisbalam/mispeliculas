package com.luisbalam.mispeliculas;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.lang.ref.WeakReference;

public class AgregarPelicula extends AppCompatActivity implements View.OnClickListener, LoaderManager.LoaderCallbacks<Cursor> {

    private long idPelicula;
    private TextInputEditText etNombrePelicula;
    private TextInputEditText etDescripcionPelicula;
    private TextInputEditText etFormatoPelicula;
    private TextInputEditText etPrecioPelicula;
    private TextInputEditText etExistenciaPelicula;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pelicula_agregar);

        etNombrePelicula =      (TextInputEditText) findViewById(R.id.etNombrePelicula);
        etDescripcionPelicula = (TextInputEditText) findViewById(R.id.etDescripcionPelicula);
        etFormatoPelicula =     (TextInputEditText) findViewById(R.id.etFormatoPelicula);
        etPrecioPelicula =      (TextInputEditText) findViewById(R.id.etPrecioPelicula);
        etExistenciaPelicula =  (TextInputEditText) findViewById(R.id.etExistenciaPelicula);

        Toast.makeText(getApplicationContext(),"Por favor, añade una Pelicula a la lista",Toast.LENGTH_SHORT).show();

        idPelicula = getIntent().getLongExtra(DetallePelicula.ID_PELICULA, -1L);

        if (idPelicula != -1L){
            getSupportLoaderManager().initLoader(0, null, this);
        }

        findViewById(R.id.btnAgregarLista).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        String nombrePelicula = etNombrePelicula.getText().toString();
        String descripcionPeli = etDescripcionPelicula.getText().toString();
        String formatoPelicula = etFormatoPelicula.getText().toString();
        String precioPelicula = etPrecioPelicula.getText().toString();
        String existenciaPelicula = etExistenciaPelicula.getText().toString();

        new CreatePeliculaTask(this, nombrePelicula, descripcionPeli, formatoPelicula, precioPelicula, existenciaPelicula, idPelicula).execute();

    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CargarPelicula(this, idPelicula);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {

        if(data != null && data.moveToFirst()){

            int nameIndex = data.getColumnIndexOrThrow(BaseDatosMisPeliculas.COLUMNA_NOMBRE);
            String nombreAnimal = data.getString(nameIndex);

            int descriptionIndex = data.getColumnIndexOrThrow(BaseDatosMisPeliculas.COLUMNA_DESCRIPCION);
            String descripcionAnimal = data.getString(descriptionIndex);

            int formatoIndex = data.getColumnIndexOrThrow(BaseDatosMisPeliculas.COLUMNA_FORMATO);
            String formatoPelicula = data.getString(formatoIndex);

            int precioIndex = data.getColumnIndexOrThrow(BaseDatosMisPeliculas.COLUMNA_PRECIO);
            String precioPelicula = data.getString(precioIndex);

            int existenciaIndex = data.getColumnIndexOrThrow(BaseDatosMisPeliculas.COLUMNA_EXISTENCIA);
            String existenciaPelicula = data.getString(existenciaIndex);

            etNombrePelicula.setText(nombreAnimal);
            etDescripcionPelicula.setText(descripcionAnimal);
            etFormatoPelicula.setText(formatoPelicula);
            etPrecioPelicula.setText(precioPelicula);
            etExistenciaPelicula.setText(existenciaPelicula);
        }

    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }


    public static class CreatePeliculaTask extends AsyncTask<Void, Void, Boolean> {

        private WeakReference<Activity> weakActivity;
        private String nombrePelicula;
        private String descripcionPeli;
        private String formatoPelicula;
        private String precioPelicula;
        private String existenciaPelicula;
        private long idPelicula;

        public CreatePeliculaTask(Activity activity, String nombrePelicula, String descripcionPeli, String formatoPelicula, String precioPelicula, String existenciaPelicula, long idPelicula){
            weakActivity = new WeakReference<Activity>(activity);
            this.nombrePelicula = nombrePelicula;
            this.descripcionPeli = descripcionPeli;
            this.formatoPelicula = formatoPelicula;
            this.precioPelicula = precioPelicula;
            this.existenciaPelicula = existenciaPelicula;
            this.idPelicula = idPelicula;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            Context context = weakActivity.get();
            if (context == null){
                return false;
            }

            Context contextoMisPeliculas = context.getApplicationContext();

            Boolean success = false;

            if (idPelicula != -1L){
                int filasAfectadas = BaseDatosMisPeliculas.actualizaAnimal(contextoMisPeliculas, nombrePelicula, descripcionPeli, formatoPelicula, precioPelicula, existenciaPelicula, idPelicula);
                success = (filasAfectadas != 0);
            } else {
                long id = BaseDatosMisPeliculas.insertaAnimal(contextoMisPeliculas, nombrePelicula, descripcionPeli, formatoPelicula, precioPelicula, existenciaPelicula);
                success = (id != -1L);
            }
            return  success;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            Activity context = weakActivity.get();
            if(context == null){
                return;
            }
            if (aBoolean){
                Toast.makeText(context, "¡Pelicula Añadida con éxito!", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(context, "Algo anda mal...", Toast.LENGTH_SHORT).show();
            }
            context.finish();
        }
    }
}
