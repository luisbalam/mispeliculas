package com.luisbalam.mispeliculas;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.support.annotation.NonNull;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.LocalBroadcastManager;

/**
 * Created by luisadrian on 11/18/16.
 */

public class CargarPelicula extends AsyncTaskLoader<Cursor> {

    private Cursor mData;
    private long idPelicula;
    private AnimalsObserver mObserver;

    public CargarPelicula(@NonNull Context context, long idPelicula) {
        super(context);
        this.idPelicula = idPelicula;
    }

    @Override
    public Cursor loadInBackground() {
        Context contexto = getContext();
        Cursor cursor = BaseDatosMisPeliculas.devuelveConId(contexto, idPelicula);

        return cursor;
    }

    @Override
    public void deliverResult(Cursor data) {
        if (isReset()) {
            releaseResources(data);
            return;
        }

        Cursor oldData = mData;
        mData = data;

        if (isStarted()) {
            super.deliverResult(data);
        }

        if (oldData != null && oldData != data) {
            releaseResources(oldData);
        }
    }

    @Override
    protected void onStartLoading() {
        if (mData != null) {
            deliverResult(mData);
        }

        if (mObserver == null) {
            mObserver = new CargarPelicula.AnimalsObserver(this);
            mObserver.register();
        }

        if (takeContentChanged() || mData == null) {
            forceLoad();
        }
    }

    @Override
    protected void onStopLoading() {
        cancelLoad();
    }

    @Override
    protected void onReset() {
        onStopLoading();

        if (mData != null) {
            releaseResources(mData);
            mData = null;
        }

        if (mObserver != null) {
            mObserver.unregister();
            mObserver = null;
        }
    }

    @Override
    public void onCanceled(Cursor data) {
        super.onCanceled(data);
        releaseResources(data);
    }

    private void releaseResources(Cursor data) {
        if (data != null){
            data.close();
        }
    }

    public static class AnimalsObserver extends BroadcastReceiver {
        private final CargarPelicula loader;

        public AnimalsObserver(CargarPelicula loader) {
            this.loader = loader;
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            loader.onContentChanged();
        }

        public void unregister(){
            LocalBroadcastManager.getInstance(loader.getContext()).unregisterReceiver(this);
        }
        public void register(){

            IntentFilter filter = new IntentFilter(CargarPeliculas.ACTION_RELOAD_TABLE);
            LocalBroadcastManager.getInstance(loader.getContext()).registerReceiver(this, filter);

        }
    }
}

