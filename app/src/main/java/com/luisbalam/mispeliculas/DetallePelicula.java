package com.luisbalam.mispeliculas;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.support.design.widget.Snackbar;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.ref.WeakReference;

public class DetallePelicula extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {

    private TextView tvMostrarNombrePelicula;
    private TextView tvMostrarDescripcionPelicula;
    private TextView tvMostrarFormatoPelicula;
    private TextView tvMostrarPrecioPelicula;
    private TextView tvMostrarExistenciaPelicula;
    private long idPelicula2;
    public static final String ID_PELICULA = "id.pelicula";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pelicula_detalle);

        tvMostrarNombrePelicula = (TextView) findViewById(R.id.tvMostrarNombrePelicula);
        tvMostrarDescripcionPelicula = (TextView) findViewById(R.id.tvMostrarDescripcionPelicula);
        tvMostrarFormatoPelicula = (TextView) findViewById(R.id.tvMostrarFormatoPelicula);
        tvMostrarPrecioPelicula = (TextView) findViewById(R.id.tvMostrarPrecioPelicula);
        tvMostrarExistenciaPelicula = (TextView) findViewById(R.id.tvMostrarExistenciaPelicula);

        Intent intentLlegada = getIntent();
        idPelicula2 = intentLlegada.getLongExtra(MainPeliculas.ID_PELICULA, -1L);

        getSupportLoaderManager().initLoader(0, null, this);

        findViewById(R.id.btnEliminarDeLista).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        new DeletePeliculaTask(DetallePelicula.this, idPelicula2).execute();
                    }
                }
        );

        findViewById(R.id.btnEditarDatosPeli).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent irAgregarPelicula = new Intent(DetallePelicula.this, AgregarPelicula.class);
                        irAgregarPelicula.putExtra(ID_PELICULA, idPelicula2);
                        startActivity(irAgregarPelicula);

                    }
                }
        );

    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CargarPelicula(this, idPelicula2);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {

        if(data != null && data.moveToFirst()){

            int peliculaIndex = data.getColumnIndexOrThrow(BaseDatosMisPeliculas.COLUMNA_NOMBRE);
            String nombrePelicula = data.getString(peliculaIndex);

            int descripcionIndex = data.getColumnIndexOrThrow(BaseDatosMisPeliculas.COLUMNA_DESCRIPCION);
            String descripcionPeli = data.getString(descripcionIndex);

            int formatoIndex = data.getColumnIndexOrThrow(BaseDatosMisPeliculas.COLUMNA_FORMATO);
            String formatoPeli = data.getString(formatoIndex);

            int precioIndex = data.getColumnIndexOrThrow(BaseDatosMisPeliculas.COLUMNA_PRECIO);
            String precioPeli = data.getString(precioIndex);

            int existenciaIndex = data.getColumnIndexOrThrow(BaseDatosMisPeliculas.COLUMNA_EXISTENCIA);
            String existenciaPeli = data.getString(existenciaIndex);

            tvMostrarNombrePelicula.setText(nombrePelicula);
            tvMostrarDescripcionPelicula.setText(descripcionPeli);
            tvMostrarFormatoPelicula.setText(formatoPeli);
            tvMostrarPrecioPelicula.setText(precioPeli);
            tvMostrarExistenciaPelicula.setText(existenciaPeli);

            Toast.makeText(getApplicationContext(),"Más detalles acerca de "+nombrePelicula,Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    public static class DeletePeliculaTask extends AsyncTask<Void, Void, Boolean> {

        private WeakReference<Activity> weakActivity;
        private long idPelicula2;

        public DeletePeliculaTask(Activity activity, long id){
            weakActivity = new WeakReference<Activity>(activity);
            idPelicula2 = id;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            Context context = weakActivity.get();
            if (context == null){
                return false;
            }

            Context contextoMisPeliculas = context.getApplicationContext();

            int filasAfectadas = BaseDatosMisPeliculas.eliminaConId(contextoMisPeliculas, idPelicula2);
            return  (filasAfectadas != 0);
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            Activity context = weakActivity.get();
            if(context == null){
                return;
            }
            if (aBoolean){
                Toast.makeText(context, "¡Pelicula eliminada con éxito!", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(context, "Algo anda mal...", Toast.LENGTH_SHORT).show();
            }
            context.finish();
        }
    }
}