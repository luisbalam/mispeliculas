package com.luisbalam.mispeliculas;

import android.content.Intent;
import android.database.Cursor;
import android.support.design.widget.Snackbar;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.widget.ScrollerCompat;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

public class MainPeliculas extends AppCompatActivity implements AdapterView.OnItemClickListener, LoaderManager.LoaderCallbacks<Cursor> {

    public final static String ID_PELICULA = "peliculas.ID_PELICULA";
    private SimpleCursorAdapter misPeliculasAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.peliculas_main);

        ListView lvMisPeliculas = (ListView) findViewById(R.id.lvMisPeliculas);
        lvMisPeliculas.setOnItemClickListener(this);

        misPeliculasAdapter = new SimpleCursorAdapter(
                this,
                android.R.layout.simple_list_item_1,
                null,
                new String[]{BaseDatosMisPeliculas.COLUMNA_NOMBRE},
                new int[]{android.R.id.text1},
                0);

        lvMisPeliculas.setAdapter(misPeliculasAdapter);

        getSupportLoaderManager().initLoader(0,null,this);

        findViewById(R.id.fabAddPelicula).setOnClickListener( new View.OnClickListener(){

            @Override
            public void onClick(View v) {

                Intent irAgregarPelicula = new Intent(MainPeliculas.this, AgregarPelicula.class);
                startActivity(irAgregarPelicula);

            }
        });



    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        Intent irDetallePelicula = new Intent(this, DetallePelicula.class);
        irDetallePelicula.putExtra(ID_PELICULA, misPeliculasAdapter.getItemId(position));
        startActivity(irDetallePelicula);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CargarPeliculas(this);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {

        misPeliculasAdapter.swapCursor(data);

    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

        misPeliculasAdapter.swapCursor(null);
    }
}
